#!/bin/bash

echo "Clearing local apt cache"

sudo apt -y autoremove

rm -f /var/cache/apt/archives/*deb
