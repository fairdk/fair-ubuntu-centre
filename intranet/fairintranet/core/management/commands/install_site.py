import logging
import os
import sys

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


from core import models
from django.core.management import call_command


logger = logging.getLogger('fairintranet.core')

DEFAULT_YAML_PATH = os.path.abspath(
    os.path.join(settings.PROJECT_ROOT, "..", "library")
)


class Command(BaseCommand):
    help = (
        'Asks for institution name etc. for a new site'
    )
    args = ''

    def add_arguments(self, parser):
        parser.add_argument(
            '--name',
            type=str,
            dest='name',
            default=None,
            help="Name of school or institution. Will be prompted if not provided.",
        )
        parser.add_argument(
            '--yaml-library',
            type=str,
            dest='yaml_libary',
            default=None,
            help="Path of YAML library files to import",
        )
        parser.add_argument(
            '--skip-yaml',
            action="store_true",
            dest='skip_yaml',
            help="Skips library import",
        )
        parser.add_argument(
            '--skip-media-check',
            action='store_true',
            dest='skip_media_check',
            help="When importing, does not check if media exists on disk",
        )

    def handle(self, *args, **options):
        try:
            page = models.HomePage.get_root_nodes()[0].get_children()[0]  # @UndefinedVariable
        except IndexError:
            logger.error("No root page")
            sys.exit(-1)
        
        if not options['name']:
            school_name = input("Enter the name of school/institution (empty = '{}'): ".format(page.title))
            school_name = school_name.strip()
            if not school_name:
                print("Using default: {}".format(page.title))
                school_name = page.title
        else:
            school_name = options['name']

        site_name = school_name
        page.title = site_name
        page.save()
        
        if models.ResourceUsage.objects.exists():
            logger.info("Resetting statistics...")
            models.ResourceUsage.objects.all().delete()
        
        path = options['yaml_libary'] or None
        while path is None and not options['skip_yaml']:
            print("Enter location of library YAML data followed by ENTER.")
            path = input("as file system path (blank={}): ".format(DEFAULT_YAML_PATH))
            path = path.strip()
            if not path:
                path = DEFAULT_YAML_PATH
            if not os.path.exists(path):
                print("This path does not exist. Please try again.")
                path = None
        
        if not options["skip_yaml"]:
            call_command("importyaml", path, overwrite=True, skip_media_check=options["skip_media_check"])
        
        if not User.objects.filter(is_superuser=True).exists():
            User.objects.create_superuser("admin", "none@offline.org", "openaccess",)
            print("Automatically creating a superuser. Username: admin, Password: openaccess")
            
        logger.info("New site name set: {:s}".format(site_name))
