type: movie
slug: looking-for-the-revolution
title: Looking for the Revolution
menu_order: 100
live: true
caption: Ché Guevara died in Bolivia trying to bring revolution to South America.
  Forty years later, his admirer Evo Morales becomes the continent's first elected
  indigenous president with the promise of continuing Che's unfinished revolution.
  Will he be able to do it?
description: '*   Producer: __Rodrigo Vazquez__

  *   Director: __Rodrigo Vazquez__

  *   Editor: __Úna Ní Dhonghaíle__

  *   Duration: __52:30__

  *   Country: __Bolivia__


  ### ABOUT THIS FILM


  It takes more than a Che Guevara t-shirt to be a revolutionary. It wasn’t branded
  images of rebellion that inspired Che – it was experiences traveling around in Latin
  America and Africa and observing the realities of rural poverty. The living conditions
  of the destitute convinced him that radical change was necessary. Today, billions
  of people still live in poverty, without access to adequate healthcare, clean water
  and food. The world’s ecosystems are under tremendous strain from human impact.
  Corruption and human rights violations still impact large parts of the human population.
  Is there a way to change these negative trends? Don’t say it’s a t-shirt. What would
  make you start a revolution


  Che Guevara died in Southern Bolivia while trying to ignite the sparks of revolution
  throughout South America. His death at the hands of Bolivian Rangers trained and
  financed by the US Government, marked the beginning of the cocaine era in Bolivia.
  Forty years later and under pressure from the masses who gave him a clear mandate,
  the first indigenous President Evo Morales (an ex-coca leaf farmer) is promising
  to continue the revolution. He has nationalised the oil industry and passed laws
  on Agrarian reform. Despite the revolutionary-sounding election speeches and campaign
  iconography that accompanied his landslide victory, on closer inspection it emerges
  that the old system is pretty much alive inside the new one. Corruption, nepotism
  and old-fashioned populism are at the core of this movement. The more Morales does
  to create employment, the more the landowners conspire against him and paralyse
  Bolivia’s economy. As a result, no jobs are created and the pressure from the poor
  increases. The cycle of tension threatens to crush both the country and the indigenous
  revolution. Looking for the Revolution is about the dynamics of that tension as
  witnessed by the characters of the film – the struggle for power between landowners
  and the indigenous movement, and the continuation of a revolution Morales-style,
  started so long ago.


  ### KEY ISSUES


  *   Socialism Versus Capitalism

  *   Unions

  *   Worker’s Rights

  *   Nationalisation


  ### DISCUSSION QUESTIONS


  *   What are your thoughts about the situation of the poor in Bolivia?

  *   What problems confronted the newly elected government?

  *   Was Esther (the union leader) really ‘on the take’, or was she a scapegoat?If
  so, why or for what purpose?

  *   Why does nothing change once Jiovanna becomes a member of Parliament?What ‘blocks’
  does she encounter?

  *   Can a democracy ‘balance’ the rights of all groups in a society?Give an example.

  *   What is/was the revolution the people were actually looking for?What were they
  seeking?

  *   What influences did the early European settlement have on the indigenous people?


  ### DIRECTOR BIOGRAPHY


  RODRIGO VAZQUEZ has trained at the National Film and Television School in the UK
  where he qualified in both Director and Camera in Documentary Studies. He has also
  obtained a BA degree in Film Direction and Theory from the Universidad del Cine
  in Argentina and holds a Film Production Diploma from Argentina’s National Film
  School. As a director and cameraman, Rodgrigo has completed numerous feature films
  and documentaries including Condor: The Axis of Evil (2003) which had it’s world
  premier at the Cannes Film Festival. He has also directed documentaries for the
  UK’s Channel 4 including five for the channel’s acclaimed Unreported World Foreign
  Affairs series.


  ### FILM CONTEXT


  Running on a platform of socialist reform, Morales gained the overwhelming support
  of Bolivia’s indigenous farming class, and is now actively working to redistribute
  land and nationalize Bolivia’s vast natural resources. However, the new president’s
  moves towards nationalization have sparked complaints among the landowning, upper
  class (mostly of Spanish heritage), renewing tensions between the rich and the poor,
  white Bolivians and indigenous Bolivians. The question remains: How will Morales’
  Bolivia be any different from the Bolivia of the past?


  ### POLITICAL HISTORY


  Bolivia, a landlocked country in South America, has a population of 8.2 million
  people. Approximately 50.6% of the population is indigenous (meaning pre-Columbus
  natives of the land), 30% mestizo (mixed Indian and white heritage), and 15% white
  (of mostly Spanish heritage). Bolivia was a Spanish colony until 1825 when it gained
  its independence, and was ruled by the land-owning white minority until 2005. Although
  rich in oil and with the second largest gas reserves on the continent, Bolivia is
  considered to be South America’s poorest country. The majority of Bolivians are
  subsistence farmers, miners, small time traders or artisans. White minority domination
  formally ended in 2005 when Morales, an indigenous former coca farmer, came to power.


  ### THE POLITICAL &nbsp;SCENE


  Bolivia has the largest indigenous population in South America, making up approximately
  two-thirds of Bolivia’s total population. Traditionally, the indigenous population
  have fallen into the poorest ranks of society, and make up the overwhelming majority
  of subsistence farmers and miners. They also account for the majority of rural dwellers
  living in the poorest conditions. Over the last ten years, Bolivia has seen much
  agitation from this sector of society, who has protested increasingly for the redistribution
  of wealth and the repossession of Bolivia’s natural resources, which the local population
  claim is too controlled by foreign entities. Hopes are now pinned on indigenous
  president Morales, a former coca farmer who has won the support of his people on
  the basis of making redistributing land and wealth to Bolivia’s poor.


  ### WORLD &nbsp;RELEVANCE


  In May 2006, Morales shocked the world and delighted his supporters by ordering
  the nationalization of Bolivia’s natural resources, requiring all foreign companies
  to be at least 51% owned by the state. The president has also promised to raise
  taxes on foreign mining companies as well as redistribute as much as one-fifth of
  Bolivia’s land to the peasant population.


  ### LOOKING AHEAD


  Under the increasingly critical eye of the United States, Morales must also deal
  with anti-drug trafficking concerns as Bolivia’s coca farmers (who farm the leaf,
  coca, which is used to produce cocaine) routinely have their crops erased by American
  anti-drug forces, temporarily destroying their means for survival. The president,
  a former coca farmer himself, is against drugs, but has promised to relax restrictions
  on growing coca, as it remains the livelihood of thousands of Bolivians and, he
  argues, has many traditional and medicinal uses.'
license: null
resource_link: http://static.fair/movies/why_democracy/looking_for_the_revolution.pls
year: null
author: Rodrigo Vazquez
country: ''
duration: '52:30'
icon_predefined: null
retrieved: null
disk_path_check: null
thumbnail_file: looking-for-the-revolution.thumb.jpg
