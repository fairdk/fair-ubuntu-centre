#!/bin/bash

echo "---------------------------------------"
echo "Installing nginx http server         "
echo "---------------------------------------"

apt-get install -y -q nginx

# Modules for nginx
apt-get install -y -q uwsgi uwsgi-plugin-python3

echo "---------------------------------------"
echo "Setting up local web server"
echo "---------------------------------------"

if [ ! -L /var/www/html/ubuntu ]
then
	echo "Creating links for our repository"
	ln -s ${FAIR_DRIVE_MOUNTPOINT}/ubuntu /var/www/html/ubuntu
	ln -s /var/www/html/ubuntu/pool /var/www/html/pool
fi

if [ ! -f /var/www/html/ubuntu/ubuntu ] && [ ! -d /var/www/html/ubuntu/ubuntu ] 
then
	ln -s /var/www/html/ubuntu/ /var/www/html/ubuntu/ubuntu
fi

echo "Installing default index.html"
copy_skel var/www/html/index.html

echo "Installing mime types for Nginx"
copy_skel etc/nginx/mime.types

echo "Creating virtual hosts for the repository and the intranet..."


rm -f /etc/nginx/sites-enabled/default

copy_skel etc/nginx/sites-available/000-default
copy_skel etc/nginx/sites-available/001-repo

copy_skel etc/nginx/sites-enabled/000-default
copy_skel etc/nginx/sites-enabled/001-repo


systemctl reload nginx

echo "---------------------------------------"
echo "Camara"
echo "---------------------------------------"


if [ -d ${FAIR_DRIVE_MOUNTPOINT}/data/camara ]
then
	if [ ! -d /var/www/html/camara ]
	then
        	echo "Creating links for Camara"
		chmod -R o+r ${FAIR_DRIVE_MOUNTPOINT}/data/camara
		chmod -R o+X ${FAIR_DRIVE_MOUNTPOINT}/data/camara
        	ln -s ${FAIR_DRIVE_MOUNTPOINT}/data/camara /var/www/html/camara
	else
		echo "Camara directory already symlinked"
	fi
else
	echo "Camara directory does not exist in the FAIR archive"
fi

echo "---------------------------------------"
echo "Distro folder (iso images of other linux)"
echo "---------------------------------------"


if [ ! -d /var/www/html/distros ] && [ -d ${FAIR_DRIVE_MOUNTPOINT}/data/distros ]
then
        echo "Creating links for linux distros"
	chmod -R o+r ${FAIR_DRIVE_MOUNTPOINT}/data/distros
	chmod -R o+X ${FAIR_DRIVE_MOUNTPOINT}/data/distros
        ln -s ${FAIR_DRIVE_MOUNTPOINT}/data/distros /var/www/html/distros
else
	echo "Linux distros directory already symlinked (or the directory does not exist in the FAIR archive)"
fi

