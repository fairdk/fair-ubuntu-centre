Background
==========

The primary goal of the project is to distribute open source software and open access knowledge and media repositories
to remote areas without relying on internet connectivity. More universally speaking, the project attempts to do ICT4D, but without any broadband internet connection in the actual deployments of the project.

Secondary goals, are efficient deployment of new centres, and maintainance of existing ones. With the software system, the main deployment effort is  physically setting up the ICT centres, the sowtware should run immediately "out of the box". 

The project provides the scripts necessary to configure an ordinary desktop installation of Ubuntu 18.04 to become a
network file server, TFTP server, and HTTP intranet server for a variety of offline resources (hereunder the full Ubuntu repositories!).

In order to use the scripts, a (possibly external) hard drive must be created with a number of required and optional resources.


Choice of Linux OS
------------------

The project is based on Ubuntu 18.04 LTS.
A basic policy is to use Ubuntu's LTS (Long-Term-Support) after it has been out for at least 6 months because it is the most well-tested, feature-rich, and beginner-friendly distibution available.


Currently
---------

The distribution of Ubuntu 18.04 supports **only 64-bit servers**. Client computers can be either 64-bit or 32-bit.

.. warning::
	On our current plans, 18.04 is the final version that will support 32-bit client computers.

Development effort has been put into modularizing the setup and facilitating customization, so that the project will be usefull beyond FAIR's deployments.

We are adding functionalities and documentation to support the workflow of technicians working in the field.

Roadmap
-------

The next upgrade will:

* Depricate 32-bit computers
* <Your ideas here!>


Discussion
----------

We do aim a lot at providing backwards compatible setups. Experiences say that school
computers are better off with less machine intensive Linux desktops, and that
newer versions do not always provide better performance. For instance, we see that Ubuntu 10.04
is actually providing better responsiveness on older hardware (256 MB, <2 GHz CPU).

As much as Ubuntu, the Linux kernel, X drivers etc. are adding support for new hardware,
older hardware is becoming less supported as the test audience shrinks.

Another alternative would be to add installation configurations that gave the choice of Lubuntu
or Xubuntu desktops.


Philosophy
----------

Everything in here is implemented in flat-out BASH scripts. It's not as if we
didn't feel like using a different language, however to a new system administrator
wanting to know what's going on, the choice of a scripting language identical
to the command line will make the process of server installation and configuration
more transparent.

.. _security-ref-label:

Security Policy
---------------
Linux is a highly secure operating system, which by default restricts permission to change files to a minimum. But software system is only as secure as the people using it. Therefore, your crew of technicians needs to agree on common policies to make sure access to changing the system is only given to trusted parties, who have proper instructions. 

Students using client computers are able to save files to their student directory, but otherwise are unable to make persistent changes to the setup. Even this limited permission to create files can be problematic because it can quickly clutter up the desktop. Therefore, to discourage this, tools are provided to clean the student directory, and restore the default setup. 

Teachers are given root access to the client computers, enabling them, for example, to install new programs. Teachers are also given user accounts on the server. Teacher are allowed to create, curate, and customize content of the intranet, after being introduced to the content management system. Schools typically do not need root access to the server. This ensures that critical files on the server cannot be corrupted.

Technicians from your team will have root access to the server to perform updates. 

