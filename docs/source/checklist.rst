Equipment check list when setting up a new center
=================================================

The following is a list of things to remember before embarking on a trip to create a new Digital Library. 

.. note:: An accurate database of the location of equipment facilitates long-term maintenance. Therefore, never let equipment pass through the doors of the warehouse without writing down its ID number and noting the equipments' new location! FAIR denmark is working on new warehouse data management system to make tracking of equipment easy. 

Tools
-----

 * Network cable crimpers
 * Screw drivers, hammer, pliers
 * LAN testers

Accessories
-----------

Make sure that you are familiar with the layout and facilities of the room in advance: how many electrical outlets are present, the location of the desks, etc. You need to know approximately how many metres of network cable to bring, and how many power extension cables. Sketch a floorplan, with the location of the computers, switches, server, printer. 



 * LAN cables (both pre-made, and unfinished rolls of raw cable for DIY)
    * 1 for each computer
    * 1 for each connection between switches
    * 1 for the server
    * 1 for printer
    
 * Ethernet plugs (RJ-45) for DIY cables
    * 2 for each cable
    * \+ 10-20% extra for mistakes

 * Power cables for desktop computers and screens
 * Power supplies for laptops
 * VGA cables
 * Power extension cables, if the electrical outlets are far from the computers
 * Projector + long VGA cable and power cable
 * Switches with enough ports for computers + server + printer + connection between switches
 * Cable clips
 * Lots of plastic strips to fixate loose cables
 * Labels and pen
 * Good mood!

Optional
~~~~~~~~

 * Speakers

Spare parts
-----------

.. hint:: It's best to setup and test the server before going. In case you are unable to test the server, make sure to bring an extra server or server data hard drive.

.. warning:: Computers can be broken in transport. Typically, 1 or 2 machines may be dead on arrival. You should bring one or two extra computers.

.. note:: Remember to update the inventory! When you are bringing spare parts out, and when you are bringing unused spare parts or broken computers back.

Not everything may go as planned, and its lighter to carry spare parts than spare
computers.

 * RAM (a variety of types)
 * Extra hard drives SATA+IDE
 * Graphics cards (in case Nvida / ATI drivers are buggy)
 * Graphics adapters to convert DVI to VGA, and a few DVI cables. Occasionally, a computer won't have a working VGA interface, and DVI can be used instead.

Computers
---------

 * Teacher computer (+ screen & keboard if not using a laptop)
 * Server (+ screen & keyboard or KVM (keyborad-video-mouse) switch)
 * Clients (+ screens if not laptop). Bring a couple of extras in case you experience problems.
 * Keyboards + Mice 

.. hint:: Most mice use USB plugs, and are compatible with all computers. But some mice use round PS-2 plugs, and are only compatible with computers with PS-2 ports. Make sure you know what kind of mice you are bringing, and if they are PS-2 mice, ensure you are bringing compatible computers. 
 
