from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

import logging
import os
import sys
import yaml

from django.core.management.base import BaseCommand

from core import models

from django.template.defaultfilters import slugify
from shutil import copyfile


logger = logging.getLogger('fairintranet.core')


type_map = {
    'collection': models.Collection,
    'externalcollection': models.ExternalCollection,
    'movie': models.Movie,
    'ebook': models.EBook,
}


class Command(BaseCommand):
    help = (
        "Reverse command: importyaml"
        "\n\n"
        "Exports current Collection data to a structure of YAML files, each "
        "describing a collection. A Collection is a resource, like Wikipedia, "
        "or a set of links (automatically populated later on with "
        "import_resource_folder)."
        "\n\n"
        "Example structure:"
        "\n\n"
        "<target dir>/\n"
        "<target dir>/resource-title.yml\n"
        "<target dir>/resource-title.icon.[png|svg]\n"
        "<target dir>/resource-title.thumbnail.[png|svg]\n"
        "<target dir>/other-resource-title.yml\n"
        "<target dir>/other-resource-title/\n"
        "<target dir>/other-resource-title/some-child.yml\n"
        "\n\n"
        "Export name conventions: Everything is lowercased and slugified."
        "\n\n"
        "These sets of YAML files can be stored anywhere and point to anything."
        "\n\n"
        "They are essentially just resource locators for the intranet, and "
        "we haven't written proper documentation for them and maybe we never "
        "will because we're just one little tiny organization doing this right "
        "now."
    )
    args = '<target dir>'

    def add_arguments(self, parser):
        parser.add_argument('target', type=str,)
        parser.add_argument(
            '--default-storage-root',
            type=str,
            dest='storage',
            default=None,
            help="Assumes a storage drive root directory, if one does not exist",
        )
        parser.add_argument(
            '--ignore-missing',
            type=bool,
            dest='ignore_missing',
            default=False,
            help="Ignores when files are missing on the data drive.",
        )

    def handle(self, *args, **options):
        
        if not os.path.isdir(options['target']):
            logger.error("Target not found: {:s}".format(options['target']))
            sys.exit(-1)
        
        root_home_pages = models.HomePage.objects.exclude(sites_rooted_here=None)
        if root_home_pages.count() > 1:
            raise Exception("Non-implemented behavior: More than one root homepage")
        
        home_page = root_home_pages.first()
        
        self.iterate_level(home_page, options['target'])

    def iterate_level(self, level, parent_dir):
        
        children = level.get_children().specific()
        
        if children.exists() and not os.path.exists(parent_dir):
            os.mkdir(parent_dir)
        
        for collection in children:
            
            if not collection._meta.model_name in type_map.keys():
                continue
            
            # Stores the slug-version of the yaml file.
            source_yaml_name = collection.source_yaml_name or slugify(collection.title)
            collection.source_yaml_name = source_yaml_name
            collection.save()

            dct = property_dict(collection)
            
            if hasattr(collection, 'icon') and collection.icon:
                file_name = os.path.split(collection.icon.file.path)[-1]
                extension = file_name.split(".")[-1].lower()
                copyfile(
                    collection.icon.file.path,
                    os.path.join(parent_dir, source_yaml_name + ".icon." + extension)
                )
            if hasattr(collection, 'thumbnail') and collection.thumbnail:
                file_name = os.path.split(collection.thumbnail.file.path)[-1]
                extension = file_name.split(".")[-1].lower()
                copyfile(
                    collection.thumbnail.file.path,
                    os.path.join(parent_dir, source_yaml_name + ".thumb." + extension)
                )
            
            save_yaml(dct, parent_dir, source_yaml_name)

            self.iterate_level(collection, os.path.join(parent_dir, source_yaml_name))


def save_yaml(dct, target_dir, slug):
    """
    Saves a dictionary to specified target_dir with file name slug.yaml
    """
    
    fname = os.path.join(target_dir, slug) + ".yml"
    
    if os.path.exists(fname):
        old_dct = yaml.full_load(open(fname, "r"))
        dct['disk_path_check'] = old_dct.get('disk_path_check', None)
    else:
        dct['disk_path_check'] = None
        
    open(fname, "w").write(
        yaml.dump(dct, encoding='utf-8', allow_unicode=True, default_flow_style=False, sort_keys=False).decode("utf-8")
    )
    

def property_dict(obj):
    
    # All known field names on Collection or Resource
    known = set()
    known.update([f.name for f in models.Collection._meta.get_fields()])
    known.update([f.name for f in models.ExternalCollection._meta.get_fields()])
    known.update([f.name for f in models.EBook._meta.get_fields()])
    known.update([f.name for f in models.Movie._meta.get_fields()])
    
    # Insert ordered (Python 3.6+)
    dct = {}
    dct['type'] = obj._meta.model_name
    dct['slug'] = str(obj.source_yaml_name)  # Because it's a SafeText obj
    dct['title'] = obj.title
    dct['menu_order'] = obj.menu_order
    dct['live'] = obj.live
    dct['caption'] = obj.caption
    dct['description'] = obj.description
    dct['license'] = obj.license
    dct['resource_link'] = getattr(obj, 'resource_link', None)
    dct['year'] = getattr(obj, 'year', None)
    dct['author'] = getattr(obj, 'author', None)
    dct['country'] = getattr(obj, 'country', None)
    dct['duration'] = getattr(obj, 'duration', None)
    dct['icon_predefined'] = getattr(obj, 'icon_predefined', None)
    dct['icon_file'] = None
    dct['thumbnail_file'] = None
    dct['retrieved'] = getattr(obj, 'retrieved', None)
    
    # This cannot be set, it's only used at import time. Should not be
    # truncated in the original file.
    dct['disk_path_check'] = None

    # Intentionally we don't use the same keys for icon and thumbnail because
    # they cannot be assigned as part of flat YAML import - they have to be
    # created as special foreign key relations for the final import.
    if hasattr(obj, 'icon') and obj.icon:
        extension = obj.icon.file.path.split(".")[-1].lower()
        dct['icon_file'] = obj.source_yaml_name + ".icon." + extension
    if hasattr(obj, 'thumbnail') and obj.thumbnail:
        extension = obj.thumbnail.file.path.split(".")[-1].lower()
        dct['thumbnail_file'] = obj.source_yaml_name + ".thumb." + extension
    
    return dct
