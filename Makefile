.PHONY: clean clean-dist dist docs help intranet intranet-db install apt-mirror
.DEFAULT_GOAL := help
define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT
BROWSER := python3 -c "$$BROWSER_PYSCRIPT"

# Needed for source commands
SHELL := /bin/bash

help:  ## Displays this help text
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

builddeps:  ## Installs build dependencies on Debian/Ubuntu
	sudo apt install python3-dev python3-venv gcc

dist: clean-dist ## Builds the distributable build/installscripts.tar.gz
	./build/installscripts.sh

clean-dist:  ## Clean dist building
	rm -rf dist

clean: clean-dist  ## Removes files from build processes
	$(MAKE) -C docs clean

lint:  ## Checks that our docs are correctly written
	$(MAKE) -C docs linkcheck

docs: clean  ## Builds docs
	$(MAKE) -C docs html
	$(BROWSER) docs/build/html/index.html

apt-mirror:  ## Creates a new or updates an existing apt mirror of Ubuntu
	sudo ./build/apt/create_update.sh

install: ## Runs the installer, setting up this machine as a server. NB! Requires to first run: make builddeps intranet
	cd src && sudo ./install.sh

intranet: ## Creates a relocatable/portable venv for Python3
	echo "Creating the Intranet"
	python3 -m venv --copies --clear intranet/venv
	sed -i '43s/.*/VIRTUAL_ENV="$(cd "$(dirname "$(dirname "${BASH_SOURCE[0]}" )")" \&\& pwd)"/' intranet/venv/bin/activate
	# sed -i '1s/.*/#!\/usr\/bin\/env python/' intranet/venv/bin/pip*
	( deactivate || true ) && intranet/venv/bin/python -m pip install -r intranet/fairintranet/requirements.txt
	find intranet/venv/bin -type f -exec sed -i '1 s/.*python/\#\!\/usr\/bin\/env\ python/' {} \;
	cd intranet && tar cvfz venv.tar.gz venv
