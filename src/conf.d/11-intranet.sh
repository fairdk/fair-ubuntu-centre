#!/bin/bash

echo "---------------------------------------"
echo "Installing intranet                    "
echo "---------------------------------------"

INTRANET_ROOT=/var/www/intranet

apt-get install uwsgi-plugin-python3 -q -y

echo "Copying intranet files and virtualenv"

# Leaving migration files behind can be dangerous so just delete everything
# for now...
rm -rf $INTRANET_ROOT/fairintranet

# Delete all the yaml sources as well
rm -rf $INTRANET_ROOT/library

mkdir -p $INTRANET_ROOT

# Creating the intranet
if [ -d ${FAIR_INSTALL_DATA}/../../intranet ]
then
	echo "Using source version of intranet"
	cp -R ${FAIR_INSTALL_DATA}/../../intranet/* $INTRANET_ROOT/
else
	cp -R ${FAIR_INSTALL_DATA}/../intranet/* $INTRANET_ROOT/
fi

source_venv="${FAIR_INSTALL_DATA}/../../intranet/venv.tar.gz"

if [ -f "${source_venv}" ]
then
	cp -R "${source_venv}" $INTRANET_ROOT/
else
	cp -R ${FAIR_INSTALL_DATA}/venv.tar.gz $INTRANET_ROOT/
fi

echo "Unpacking virtualenv"
cd $INTRANET_ROOT
tar xfz venv.tar.gz
cd -

copy_skel etc/nginx/sites-available/002-intranet $INTRANET_ROOT
copy_skel etc/uwsgi/apps-available/intranet.ini $INTRANET_ROOT
copy_skel etc/nginx/sites-enabled/002-intranet
copy_skel etc/uwsgi/apps-enabled/intranet.ini

# Activate virtualenv
python_bin="$INTRANET_ROOT/venv/bin/python"

# Ensure that the database is migrated
echo "Creating/updating database schema"
$python_bin $INTRANET_ROOT/fairintranet/manage.py migrate --noinput --settings=fairintranet.settings.production

# Create a new site, if none
$python_bin $INTRANET_ROOT/fairintranet/manage.py install_site --settings=fairintranet.settings.production

# Import YAML resource descriptions for all the contents
# Overwrite because we want to distribute updates without too much fuzz.
$python_bin $INTRANET_ROOT/fairintranet/manage.py importyaml $INTRANET_ROOT/library --overwrite --settings=fairintranet.settings.production

# Run Django management scripts
echo "Populating static files for intranet"
$python_bin $INTRANET_ROOT/fairintranet/manage.py collectstatic --noinput --settings=fairintranet.settings.production

systemctl reload nginx.service
systemctl restart uwsgi.service

# For file uploads
chmod -R 777 $INTRANET_ROOT/media/
# Needed for populating CACHE
chmod -R 777 $INTRANET_ROOT/fairintranet/static/

chown -R www-data.www-data $INTRANET_ROOT/

echo "Symlinking resources to the main server root"
rm -f /var/www/html/movies
rm -f /var/www/html/ebooks
ln -s ${FAIR_DRIVE_MOUNTPOINT}/data/movies /var/www/html/movies
ln -s ${FAIR_DRIVE_MOUNTPOINT}/data/ebooks /var/www/html/ebooks
chmod -R o+r ${FAIR_DRIVE_MOUNTPOINT}/data/movies
chmod -R o+X ${FAIR_DRIVE_MOUNTPOINT}/data/movies
chmod -R o+r ${FAIR_DRIVE_MOUNTPOINT}/data/ebooks
chmod -R o+X ${FAIR_DRIVE_MOUNTPOINT}/data/ebooks

if [ -d ${FAIR_DRIVE_MOUNTPOINT}/data/movies/why_democracy/ ]
then
	echo "Patching up why democracy .pls files"
	sed -i 's/var\/why\_democracy/var\/movies\/why_democracy/g' ${FAIR_DRIVE_MOUNTPOINT}/data/movies/why_democracy/*pls
fi
