#!/bin/sh

# This is downloaded from the repository server and executed in a chroot'ed
# environment during debian-install.

# Remove the default display manager gdm3
apt-get remove -y gdm3

# Install lightdm instead, it's easier to configure and simpler to look at
apt-get install -y lightdm

# Download and unpack postinstall scripts
mkdir /root/postinstall
cd /root/postinstall
wget --tries=100 http://192.168.10.1/postinstall.tar.gz -O /root/postinstall/postinstall.tar.gz
tar xvfz postinstall.tar.gz

# Set the postinstall scripts to be run on first boot
echo "#!/bin/sh" > /etc/rc.local
echo "echo ''" >> /etc/rc.local
echo "echo 'Now running /root/postinstall/postinstall.sh...'" >> /etc/rc.local
echo "echo ''" >> /etc/rc.local
echo "/root/postinstall/postinstall.sh 2>&1 | tee -a /var/log/postinstall.log" >> /etc/rc.local

chmod +x /etc/rc.local

echo "options i915 modeset=1" > /etc/modprobe.d/i915.conf
