#!/bin/bash

# Enable tracebacks
# Save my own path
REPO_ROOT="`readlink -e $0`"
REPO_ROOT="`dirname $REPO_ROOT`"
SCRIPTPATH="`dirname $REPO_ROOT`"

set -eu
bash "$SCRIPTPATH/src/traceback.sh"

BRANCH=${1:-$(git symbolic-ref HEAD | sed 's/refs\/heads\///')}

echo "Building on current branch '$BRANCH'..."
echo ""

# Ensure that dist/
mkdir dist/

git archive $BRANCH src/ | tar x --ignore-command-error -C dist/
# Command can only extract into a clean dir, so move afterwards
mv dist/src dist/installscripts

git archive $BRANCH intranet/ | tar x --ignore-command-error -C dist/
# Command can only extract into a clean dir, so move afterwards
mv dist/intranet/ dist/installscripts/

# Copy the virtualenv for the intranet
cp intranet/venv.tar.gz dist/installscripts/data/
cd dist
echo "Creating tarball..."
tar cz -f installscripts-`date +'%Y%m%d'`.tar.gz installscripts

echo ""
echo "Final result stored in dist/installscripts.tar.gz"
