echo "---------------------------------------"
echo "Ping clients (automatically turn off server) "
echo "---------------------------------------"

echo "Installing ping_clients.py"

copy_skel etc/init.d/auto_shutdown
copy_skel usr/bin/ping_clients.py

chmod +x /etc/init.d/auto_shutdown
chmod +x /usr/bin/ping_clients.py

# Enable but DON'T START - that'll shut down the server
systemctl daemon-reload
systemctl enable auto_shutdown
