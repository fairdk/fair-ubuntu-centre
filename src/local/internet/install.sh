#!/bin/bash

copy_skel etc/netplan/98-internet.sh
copy_skel etc/forward.sh

chmod +x /etc/forward.sh

# This will not work, let's make a systemd script
echo "/etc/forward.sh" >> /etc/rc.local
